# Basic install:
vue create vue-d3-app2

Please pick a preset: Default (Vue 3) ([Vue 3] babel, eslint)

# Python server:
python -m http.server 9000

# install D3, bootstrap, bootswatch, vuex
- copy older package.json
yarn install

# vuex
- create src/store/index.js

# main.js
- copy from older

# App.vue
- copy from older