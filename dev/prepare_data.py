"""Prepare data from calculated values."""

import json

path = "../src/assets/"

fname = "current_seats.json"

with open(path + fname) as fin:
    original = json.load(fin)

parties = []
candidates = []

parts = {
    'lo': 'core',
    'seats': 'model',
    'hi': 'potential'
}
prev_parts = {
    'seats': 'lo',
    'hi': 'seats'
}

# Seznam Zpravy colors
def szc(c):
    if c == 'ANO':
        return "#43508C"
    if c == 'Piráti+STAN':
        return "#222222"
    if c == 'SPOLU':
        return '#4175E0'
    if c == 'ČSSD':
        return '#f9932e'
    if c == 'KSČM':
        return '#e2354b'
    if c == 'SPD':
        return ' #c05f2f'
    if c == 'Přísaha':
        return '#1f4bff'

i = 1
j = 1
for row in original['data']:
    if row['name'] == 'Piráti+STAN':
        party_name = 'Piráti+STAN' #'PirSTAN'
    else:
        party_name = row['name']
    party = {
        'name': party_name,
        'rank': i,
        'color': szc(row['name']), # row['color'],
        'core': int(row['lo']),
        'model': int(row['seats']),
        'potential': int(row['hi']),
        'selected': (i == 1)
    }
    parties.append(party)
    
    p = 0
    j = 1
    for k in parts:
        if k in prev_parts.keys():
            rang = int(row[k] - row[prev_parts[k]])
        else:
            rang = int(row[k])
        for m in range(0, rang):
            candidate = {
                'party': party_name,
                'color': szc(row['name']), # row['color'],
                'opacity': 1 - p * 0.6,
                'total_rank': j,
                'selected': (i == 1),
                'level': p,
                # 'position': {'x': 0, 'y': 0},
                'party_rank': i,
                'local_rank': m + 1
            }
            candidates.append(candidate)
            j += 1
        p += 1

    i += 1

len(candidates)
# candidates[0:5]

with open(path + "parties.json", "w") as fout:
    json.dump(parties, fout)

candidates = sorted(candidates, key=lambda x: x['opacity'], reverse=True)

with open(path + "candidates.json", "w") as fout:
    json.dump(candidates, fout)
