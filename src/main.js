import { createApp } from 'vue'
import App from './App.vue'
import store from './store'

import "bootstrap"
import "bootswatch/dist/lumen/bootstrap.min.css"


const app = createApp(App)

app
.use(store)

app.mount('#app')