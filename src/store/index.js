import { createStore } from 'vuex';
import parties from '../assets/parties.json'
import candidates from '../assets/candidates.json'

export default createStore({
  state: {
    parties: parties,
    candidates: candidates,
    test: 0
  },
  getters: {
    getCandidates: function(state) {
      return state.candidates
    },
    getTest: function(state) {
      return state.test
    }
  },
  mutations: {
    // clicked any party button
    clicked: function(state, payload) {
      for (let i = 0; i < state.candidates.length; i++) {
        if (state.candidates[i]['party'] == payload.name) {
          state.candidates[i]['selected'] = !(state.candidates[i]['selected'])
        }
      }
      state.parties[payload.rank - 1].selected = !(state.parties[payload.rank - 1].selected)
    }
  },
  actions: {},
});